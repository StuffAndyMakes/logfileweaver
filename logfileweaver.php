<?php

require_once('LogFileWeaver.class.php');

$weaver = new LogFileWeaver();

try {
	$weaver->addLogFile(new LogFile('access.log', '/\[(([0-9]+)\/([a-zA-Z]{3})\/([0-9]{4}):([0-9]{2}):([0-9]{2}):([0-9]{2}) \+0000)\]/', 'j/M/Y:H:i:s O'));
	$weaver->addLogFile(new LogFile('error.log', '/^\[(([A-Za-z]{3}) ([a-zA-Z]{3}) ([0-9]+) ([0-9]{2}):([0-9]{2}):([0-9]{2}) ([0-9]{4}))\]/', 'D M j H:i:s Y'));
	$weaver->addLogFile(new LogFile('php-errors.log', '/^\[(([0-9]+)-([a-zA-Z]{3})-([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2}) ([A-Za-z_\/]+))\]/', 'j-M-Y H:i:s e'));
	$weaver->addLogFile(new LogFile('drupal-requests.log', '/(([0-9]+)\/([a-zA-Z]{3})\/([0-9]{4}):([0-9]{2}):([0-9]{2}):([0-9]{2}) \+0000)/', 'j/M/Y:H:i:s O'));
	$weaver->addLogFile(new LogFile('drupal-watchdog.log', '/(([a-zA-Z]{3}) ([0-9]+) ([0-9]{2}):([0-9]{2}):([0-9]{2}))/', 'M d H:i:s'));
	$weaver->addLogFile(new LogFile('drush-cron.log', '/^\[(([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2}) ([A-Z]{3}))\]/', 'Y-m-d H:i:s T'));
	$weaver->addLogFile(new LogFile('fpm-access.log', '/(([0-9]+)\/([a-zA-Z]{3})\/([0-9]{4}):([0-9]{2}):([0-9]{2}):([0-9]{2}) \+0000)/', 'j/M/Y:H:i:s O'));
	$weaver->addLogFile(new LogFile('fpm-error.log', '/^\[(([0-9]+)-([a-zA-Z]{3})-([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2}))\]/', 'j-M-Y H:i:s'));
} catch (Exception $e) {
	fputs(STDOUT, 'Exception creating Logfile object: ' . $e->getMessage() . "\n");
}

$weaver->weave('allthe.log');

$weaver->close();
