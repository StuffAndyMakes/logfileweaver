<?php

class LogFile {

	private $logFilename = '';
	private $timestampRegEx = '';
	private $timestampFormat = '';
	private $fileHandle = NULL;
	private $currentLine = '';
	private $lineNumber = -1;

	function __construct(string $fn = '', string $rx = '', string $fmt = '') {
		if (empty($fn)) {
			throw new Exception('Logfile filename cannot be empty.', 1);
		}
		if (empty($rx)) {
			throw new Exception('Timestamp RegEx cannot be empty.', 1);
		}
		if (empty($fmt)) {
			throw new Exception('Timestamp format cannot be empty.', 1);
		}
		$this->logFilename = $fn;
		$this->fileHandle = fopen($this->logFilename, 'r');
		if ($this->fileHandle == FALSE) {
			throw new Exception('Could not open file "' . $this->logFilename . '"', 1);
		}
		$this->timestampRegEx = $rx;
		$this->timestampFormat = $fmt;
		$this->nextLine(); // load the first line in
	}

	public function getLogFilename() {
		return $this->logFilename;
	}

	public function rewind() {
		rewind($this->fileHandle);
	}

	public function hasMoreLines() {
		return !feof($this->fileHandle);
	}

	public function getLineNumber() {
		return $this->lineNumber;
	}

	/* reads next line in file and increments line number
	 * pass TRUE if you want it to skip lines from which a timestamp cannot be determined
	 * returns TRUE if it pulled the next line OK, FALSE if EOF
	 */
	public function nextLine(bool $skipBadLines = FALSE) {
		if (feof($this->fileHandle)) {
			$this->currentLine = ''; // clear last line, no more to get from file
			return FALSE;
		}
		if ($skipBadLines) {
			do {
				$this->lineNumber++;
				$this->currentLine = fgets($this->fileHandle);
			} while ($this->getTimestamp() == NULL && !feof($this->fileHandle));
			if ($this->getTimestamp() != NULL) {
				return TRUE;
			}
			if (feof($this->fileHandle)) {
				$this->currentLine = ''; // clear last line, no more to get from file
				return FALSE;
			}
		}
		$this->lineNumber++;
		$this->currentLine = fgets($this->fileHandle);
		return TRUE;
	}

	public function getCurrentLine() {
		return $this->currentLine;
	}

	/* tries to determine the timestamp from the current line
	 * returns NULL if it cannot, otherwise returns a DateTime object
	 */
	public function getTimestamp() {
		if (preg_match($this->timestampRegEx, $this->currentLine, $matches) === FALSE) {
			return NULL;
		}
		$ts = DateTime::createFromFormat($this->timestampFormat, $matches[1]);
		if ($ts == FALSE) {
			return NULL;
		}
		$ts->setTimezone(new DateTimeZone('UTC'));
		return $ts;
	}

	public function close() {
		if ($this->fileHandle != NULL) {
			fclose($this->fileHandle);
		}
	}

}
