# LogFileWeaver

LogFileWeaver is a PHP script that can take multiple log files from wherever and waeve them together into s single file with the entries of the other files interleaved in chronological order. The purpose of this is to make it easier to read events logged from different services in different log files.

I wrote this originally to help me with troubleshooting a weird issue with a Drupal application stack on a hosted service. At one point, I had eight or so different log files open in an editor. Finding entries in those log files and lining them up to work through how things went down was tedious and difficult. This script pulled all those lines together and made it easy to step through events from all over in one place.

## How To

The main file is `logfileweaver.php` and this is where I have instantiated the weaver in my environment. To run it, change it as needed for your setup and simply do the PHP command line thing:

```
php logfileweaver.php
```

To configure each `LogFile` object, you just have to provide a filename, a regular expression (I used `preg_match`), and a `date()` format string that is used to interpret the parsed timestamp string out of the logfile line and turn it into a proper timestamp (`DateTime` object).

Here is an example of instantiating one of the logfiles for the weaver to use:

```
new LogFile('access.log', '/\[(([0-9]+)\/([a-zA-Z]{3})\/([0-9]{4}):([0-9]{2}):([0-9]{2}):([0-9]{2}) \+0000)\]/', 'j/M/Y:H:i:s O')
```

The typical `access.log` file entry looks like this:

```
127.1.2.3 - - [15/Aug/2018:20:50:23 +0000] "GET /get-quote HTTP/1.1" 301 5223 "https://mysamplesite.com/products/toys" "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko" vhost=mysamplesite.prod.hosting-service.com host=mysamplesite.com hosting_site=mysite pid=1434 request_time=59111 forwarded_for="127.0.0.1" request_id="v-d4ade928-a0cc-11e8-b0d3-069135d2c7cc" 
```

This regular expression is used to find the timestamp in the entry string above:

```
/\[(([0-9]+)\/([a-zA-Z]{3})\/([0-9]{4}):([0-9]{2}):([0-9]{2}):([0-9]{2}) \+0000)\]/
```

This `date()` format string takes what the regular expression finds and allows the `DateTime::createFromFormat()` method to make a good time object out of it:

```
j/M/Y:H:i:s O
```

The `LogFile` class keeps track of which line it's on. The `nextLine()` method will return `FALSE` once the end of the file is reached and the `getCurrentLine()` method will return an empty string.

The `LogFileWeaver` class lines up all the `LogFile` object it has and continually sorts all the `getTimestamp()` values and pushes the "youngest" into the combination log file it's building for you. When it pushes an entry to the big file, it gets the next line for the logfile whose entry was copied and resorts the list again. It continues this until all logfiles hit EOF.

## Future Ideas

- I am working on adding options to change the window of time to filter out of the log files being parsed.
- I'd also like to be able to group related entries together and separate the grouped entries with spaces or lines or something.
- The script should also mark the start time and the end time of the run for fun.
- Also, wouldn't mind a status indicator or spinner or progress bar while it is parsing.

## Contributing or Suggestions

Please feel free to reach out to me if you find this thing and you find it interesting or useful.

