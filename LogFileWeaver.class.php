<?php

require_once('LogFile.class.php');


class LogFileWeaver {

	private $logFiles = [];

	public function addLogFile(LogFile $lf = NULL) {
		if ($lf == NULL) {
			throw new Exception('Cannot add a NULL LogFile to LogFileWeaver', 1);
		}
		array_push($this->logFiles, $lf);
	}

	private function endOfAllFiles() {
		foreach ($this->logFiles as $logFile) {
			if ($logFile->hasMoreLines()) {
				return FALSE;
			}
		}
		return TRUE;
	}

	static function tsCompare(LogFile $a, LogFile $b) {
		if ($a->getTimestamp()->format("U") == $b->getTimestamp()->format("U")) {
			return 0;
		}
		return ($a->getTimestamp()->format("U") < $b->getTimestamp()->format("U")) ? -1 : 1;
	}

	public function weave(string $outfile = 'weaved.log') {
		
		if (count($this->logFiles) < 2) {
			throw new Exception('Need two or more logfiles to weave together', 1);
		}
		if (empty($outfile)) {
			throw new Exception('Need an output file to weave logs into.', 1);
		}

		$output = fopen($outfile, 'w');

		fputs($output, "\n");

		$tempList = $this->logFiles;
		while (!$this->endOfAllFiles()) {
			usort($tempList, array("LogFileWeaver", "tsCompare"));
			fputs($output, $tempList[0]->getTimestamp()->format('Y-m-d H:i:s') . ' ' . $tempList[0]->getLogFilename() . ':' . $tempList[0]->getLineNumber() . ' ' . trim($tempList[0]->getCurrentLine()) . "\n");
			if ($tempList[0]->nextLine(TRUE) == FALSE) {
				array_shift($tempList);
			}
		}
		
		fputs($output, "\n");

		fclose($output);

	}

	public function close() {
		if (count($this->logFiles) > 0) {
			foreach ($this->logFiles as $logFile) {
				try {
					$logFile->close();
				} catch (Exception $e) {
					fputs(STDOUT, 'Exception closing logfile: ' . $e->getMessage() . "\n");
				}
			}
		}
	}

}
